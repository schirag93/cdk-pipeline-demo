#!/usr/bin/env node
import 'source-map-support/register';
import { CdkPipelineDemoStack } from '../lib/cdk-pipeline-demo-stack';
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';

const app = new cdk.App();
new CdkPipelineDemoStack(app, 'CdkPipelineDemoStack', {
  env: {
    account: '336640351572',
    region: 'us-east-1',
  }
});

app.synth();