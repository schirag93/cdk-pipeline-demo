import { Stack, StackProps } from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as cdk from '@aws-cdk/core';
import { CodePipeline, CodePipelineSource, ShellStep } from '@aws-cdk/pipelines';


export class CdkPipelineDemoStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const pipeline = new CodePipeline(this, 'Pipeline', {
      pipelineName: 'MyPipeline',
      synth: new ShellStep('Synth', {
        input: CodePipelineSource.connection("schirag93/cdk-pipeline-demo", "main", {
          connectionArn: "arn:aws:codestar-connections:us-east-1:336640351572:connection/a71ccbc1-ab5d-4ec4-a249-f2c69cfc2e8f"
        }),
        commands: ['npm ci', 'npm run build', 'npx cdk synth']
      })
    });
  }
}