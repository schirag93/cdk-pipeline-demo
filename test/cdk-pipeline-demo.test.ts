import * as cdk from 'aws-cdk-lib';
import * as CdkPipelineDemo from '../lib/cdk-pipeline-demo-stack';

test('Empty Stack', () => {
    const app = new cdk.App();
    // WHEN
    const stack = new CdkPipelineDemo.CdkPipelineDemoStack(app, 'MyTestStack');
    // THEN
    const actual = app.synth().getStackArtifact(stack.artifactId).template;
    expect(actual.Resources ?? {}).toEqual({});
});
